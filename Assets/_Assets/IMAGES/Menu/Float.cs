﻿using UnityEngine;
using System.Collections;

public class Float : MonoBehaviour {

	SphereCollider sc;
	float r;
	public Vector3 offset;
	void Awake()
	{
		TweenPosition tw = GetComponent<TweenPosition>();
		tw.from = transform.localPosition;
		tw.to = tw.from - offset;
	}
	void Start () {
		sc = GetComponent<SphereCollider>();
		r = sc.radius;
		
	}
	void Update()
	{
		sc.radius = r * transform.localScale.x;
	}
}
