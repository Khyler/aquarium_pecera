﻿using UnityEngine;
using System.Collections;

public class NotificationsOnOff : MonoBehaviour {
	public UILabel text;
	void Awake()
	{
		text.text = Language.Get("NOTIFICATIONS") + " "+ ( (LoadSaveData.Notifications)?"ON":"OFF");
	}
	public void ChangeNotification()
	{
		LoadSaveData.Notifications = !LoadSaveData.Notifications;
		text.text = Language.Get("NOTIFICATIONS") + " "+ ( (LoadSaveData.Notifications)?"ON":"OFF");
		StartCoroutine(NotificationChangeOnOff());
	}

	IEnumerator NotificationChangeOnOff()
	{
		string url = LoadSaveData.URL+"updateUserData.php?id="+LoadSaveData.id+"&field=Notifications&data="+ ( (LoadSaveData.Notifications)?"1":"0" );
		WWW download = new WWW(url);
		Debug.Log("URL: "+url);
		
		// Wait until the download is done
		yield return download;
		
		if(download.error != null) {
			print( "Error downloading: " + download.error );
			yield return null;
		} else {
			//text.text = Language.Get("NOTIFICATIONS") + " "+ ( (LoadSaveData.Notifications)?"ON":"OFF");
			Debug.Log("Notifications Updated.");
		}
	}
}
