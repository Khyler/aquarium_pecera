using UnityEngine;
using System.Collections;
/***/
using System.Collections.Generic;
using Prime31;
/***/
/*using System.Runtime.Serialization.Formatters.Binary; 
using System.IO;*/


public class LoadSaveData : MonoBehaviour {

	public static Data scores;
	private static LoadSaveData _instance;
	public static string URL= "http://192.168.1.20/acuarium/";
	public static LoadSaveData instance
	{
		get
		{
			if(_instance == null)
			{
				_instance = GameObject.FindObjectOfType<LoadSaveData>();
				
				//Tell unity not to destroy this object when loading a new scene!
				DontDestroyOnLoad(_instance.gameObject);
			}
			
			return _instance;
		}
	}

	//public static Data scores;
	public static string id;
	public static bool Notifications = true;
	void Awake() 
	{
		if(_instance == null)
		{
			//If I am the first instance, make me the Singleton
			_instance = this;
			DontDestroyOnLoad(this);
		}
		else
		{
			//If a Singleton already exists and you find
			//another reference in scene, destroy it!
			if(this != _instance)
				Destroy(this.gameObject);
		}
		scores = new Data();
#if UNITY_EDITOR
		id = "10206020398822136";
#endif
/*if(Application.isEditor)
		id = "341205";*/

		LoadData();
	}

	public void LoadData()
	{/**************************************
		//Open file if exists
		if(File.Exists(Application.persistentDataPath + "/data.dat"))
		{
			BinaryFormatter bf = new BinaryFormatter();
			FileStream file = File.Open (Application.persistentDataPath + "/data.dat", FileMode.Open);

			scores = (Data)bf.Deserialize(file);
			file.Close();
			 
		}
		else
		{
			scores.Pair4BestTime 	  = -1f;
			scores.Pair8BestTime 	  = -1f;
			scores.Pair12BestTime	  = -1f;
			scores.RaceShortBestTime  = -1f;
			scores.RaceMediumBestTime = -1f;
			scores.RaceLongBestTime   = -1f;
			scores.ShootMaxScore 	  = -1;
		}
		************************************************/
		StartCoroutine(LoadScores());
	}


	public static IEnumerator SaveScore(string Game, string score, bool less_better = true)
	{
		for(int i= 0; i< 99; ++i){	Debug.Log (id.ToString());}
		string url =LoadSaveData.URL+"saveScore.php?idGame="+Game+"&score="+score+"&less_better="+ ( (less_better)?"1":"0" )+"&id="+ /*user id*/id;
		WWW download = new WWW(url);
		Debug.Log("URL: "+url);
		
		// Wait until the download is done
		yield return download;
		
		if(download.error != null)
		{
			print( "Error downloading: " + download.error );
			yield return null;
		}
		else
		{
			// show the highscores
			Debug.Log(download.text);
			LoadSaveData.instance.StartCoroutine(LoadScores());
		}
	}

	public static IEnumerator LoadScores()
	{
		string url = LoadSaveData.URL+"loadScores.php?id="+ /*user id*/id;
		WWW download = new WWW(url);
		Debug.Log("URL: "+url);
		
		// Wait until the download is done
		yield return download;
		
		if(download.error != null)
		{
			print( "Error downloading: " + download.error );
			yield return null;
		} 
		else 
		{
			LoadSaveData.scores = new Data();
			LoadSaveData.scores = Json.decode<Data>(download.text);
			Debug.Log(download.text);
		}
	}
	/* NOT USED public static void SaveData()
	{
		BinaryFormatter bf = new BinaryFormatter();
		FileStream file = File.Create(Application.persistentDataPath + "/data.dat");

		bf.Serialize(file, scores);
		file.Close();

	}*/

	[System.Serializable]
	public class Data
	{
		//public Data(){}
		public Data()
		{
			Pair4BestTime = -1f;
			Pair8BestTime = -1f;
			Pair12BestTime = -1f;
			RaceShortBestTime = -1f;
			RaceMediumBestTime = -1f;
			RaceLongBestTime = -1f;
			ShootMaxScore = -1;
		}
		public int ShootMaxScore;
		public float RaceShortBestTime;
		public float RaceMediumBestTime;
		public float RaceLongBestTime;
		public float Pair4BestTime;
		public float Pair8BestTime;
		public float Pair12BestTime;
	}
}
