﻿using UnityEngine;
using System.Collections;

public class OrthoCameraZoom : MonoBehaviour {

	private Camera camera;

	public float maxZoom=1;
	public float minZoom=0.5f;

	public float sensitivity=1;

	private float pinchLength=0;
	private float lastZoom=1;
	private float newZoom=1;

	private CameraRestriction cameraRestriction;
	
	
	void Start(){
		cameraRestriction=GetComponent<CameraRestriction> ();		
		camera = GetComponent<Camera> ();
	}
	
	// Update is called once per frame
	void Update () {

		if (Input.GetAxis("Mouse ScrollWheel") > 0) // forward
		{
			newZoom = Mathf.Max (minZoom,lastZoom-0.1f);
			camera.orthographicSize=newZoom;
			lastZoom =newZoom;
		}

		if (Input.GetAxis("Mouse ScrollWheel") < 0) // forward
		{
			newZoom = Mathf.Min (maxZoom,lastZoom+0.1f);
			camera.orthographicSize=newZoom;
			lastZoom =newZoom;
			
			cameraRestriction.CorrectPosition();
		}


		if (Input.touchCount == 2 && Input.GetTouch(1).phase == TouchPhase.Began){
			
			pinchLength = Vector2.Distance(Input.GetTouch(0).position, Input.GetTouch(1).position);
			
		}
		
		if(Input.touchCount == 2 && (Input.GetTouch(0).phase == TouchPhase.Moved || Input.GetTouch(1).phase == TouchPhase.Moved)){
			
			float deltaLength = Vector2.Distance(Input.GetTouch(0).position, Input.GetTouch(1).position);
			newZoom = Mathf.Min (maxZoom,lastZoom + (pinchLength - deltaLength)*(sensitivity/1000));
			newZoom = Mathf.Max (minZoom,newZoom);
			camera.orthographicSize=newZoom;
			
			cameraRestriction.CorrectPosition();
			
		}
		
		if(Input.touchCount == 2 && (Input.GetTouch(0).phase == TouchPhase.Ended || Input.GetTouch(1).phase == TouchPhase.Ended)){
			
			lastZoom =newZoom;
			
		}
	}
	
	
}
