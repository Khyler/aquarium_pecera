﻿using UnityEngine;
using System.Collections;
using System;
using System.IO;
using Prime31;

#if UNITY_ANDROID
using FacebookAccess = Prime31.FacebookAndroid;
#elif UNITY_IPHONE
using FacebookAccess = Prime31.FacebookBinding;
#endif





public class FacebookScreenShot : MonoBehaviour
{


    public UIPanel controlsPanel;
    public InfoManager infoManager;

    private static string screenshotFilename = "someScreenshot.png";
	private static string fullPath;
    private GameManager manager;

	private Texture2D image;

    void Start()
    {
        fullPath = Application.persistentDataPath + "/" + screenshotFilename;

#if UNITY_ANDROID
        if (File.Exists(fullPath)) { 
            File.Delete(fullPath);
        }

        FacebookAccess.init(true);
        Facebook.instance.debugRequests = true;
#endif

#if UNITY_IPHONE
		if (File.Exists(fullPath)) { 
			File.Delete(fullPath);
		}
		
		FacebookAccess.init();
		Facebook.instance.debugRequests = true;
#endif
    }



#if UNITY_ANDROID || UNITY_IPHONE
    void OnEnable()
    {
        FacebookManager.sessionOpenedEvent += sessionOpenedEvent;
        FacebookManager.loginFailedEvent += loginFailedEvent;
    }

    void OnDisable()
    {
        FacebookManager.sessionOpenedEvent -= sessionOpenedEvent;
        FacebookManager.loginFailedEvent -= loginFailedEvent;
    }
#endif


#if !UNITY_ANDROID && !UNITY_IPHONE
    public void accion()
    { }
#endif


#if UNITY_ANDROID


    public void accion()
    {

        if (FacebookAndroid.isSessionValid())
        {
            sessionOpenedEvent();
        }
        else
        {
            FacebookAndroid.setSessionLoginBehavior(FacebookSessionLoginBehavior.SUPPRESS_SSO);
            FacebookAndroid.setDefaultAudience(FacebookSessionDefaultAudience.Friends);
            FacebookAndroid.loginWithPublishPermissions(new string[] { "publish_actions" });
        }
    }

    void completionHandler(string error, object result)
    {
        if (error != null)
            Debug.LogError(error);
        else
            Prime31.Utils.logObject(result);
    }

    void sessionOpenedEvent()
    {
        Debug.Log("Facebook login success");

        StartCoroutine(esperaCaptura());
    }

    void loginFailedEvent(Prime31.P31Error error)
    {
        infoManager.ToogleMessage();

        infoManager.SetMessage("FACEBOOK_LOGIN_FAILURE", null);
		infoManager.ToogleMessage ();
    }
    
    IEnumerator esperaCaptura(){

        
        Application.CaptureScreenshot(screenshotFilename);
        
        while (!File.Exists(fullPath))
        {
            yield return new WaitForSeconds(0.1f);
        }
        yield return new WaitForSeconds(0.1f);

		byte[] fileData = File.ReadAllBytes (fullPath);
		image = new Texture2D (900, 500);
		image.LoadImage (fileData);

		infoManager.SetQuestion("PUBLISH_ON_FACEBOOK?", image, new EventDelegate(this, "AcceptPublication"), new EventDelegate(this, "CancelPublication"));
        infoManager.ToogleQuestion();

    }

    public void AcceptPublication(){

        infoManager.ToogleQuestion();
        try{
        byte[] bytes = System.IO.File.ReadAllBytes(fullPath);
		Facebook.instance.postImage(bytes, Language.Get("FACEBOOK_PUBLICATION_TEXT")  , completionHandler);
		infoManager.SetMessage("PUBLISHED_ON_FACEBOOK", image);

		}catch(Exception){		
			infoManager.SetMessage("NOT_PUBLISHED_ON_FACEBOOK", null);
		}finally{		
			File.Delete (fullPath);
		}
       
		infoManager.ToogleMessage();

    }

    public void CancelPublication(){        
        File.Delete(fullPath);

        infoManager.ToogleQuestion();
    }   


#elif UNITY_IPHONE

	public void accion()
	{
		
		if (FacebookAccess.isSessionValid())
		{
			sessionOpenedEvent();
		}
		else
		{
			FacebookAccess.setSessionLoginBehavior(FacebookSessionLoginBehavior.WithNoFallbackToWebView);
			FacebookAccess.reauthorizeWithPublishPermissions( new string[] { "publish_actions" }, FacebookSessionDefaultAudience.Friends );
		}
	}
	
	void completionHandler(string error, object result)
	{
		if (error != null)
			Debug.LogError(error);
		else
			Prime31.Utils.logObject(result);
	}
	
	void sessionOpenedEvent()
	{
		Debug.Log("Facebook login success");
		
		StartCoroutine(esperaCaptura());
	}
	
	void loginFailedEvent(Prime31.P31Error error)
	{
		infoManager.ToogleMessage();
		
		infoManager.SetMessage("FACEBOOK_LOGIN_FAILURE", null);
		infoManager.ToogleMessage ();
	}
	
	IEnumerator esperaCaptura(){
		
		
		Application.CaptureScreenshot(screenshotFilename);
		
		while (!File.Exists(fullPath))
		{
			yield return new WaitForSeconds(0.1f);
		}
		yield return new WaitForSeconds(0.1f);
		
		byte[] fileData = File.ReadAllBytes (fullPath);
		image = new Texture2D (900, 500);
		image.LoadImage (fileData);
		
		infoManager.SetQuestion("PUBLISH_ON_FACEBOOK?", image, new EventDelegate(this, "AcceptPublication"), new EventDelegate(this, "CancelPublication"));
		infoManager.ToogleQuestion();
		
	}
	
	public void AcceptPublication(){
		
		infoManager.ToogleQuestion();
		try{
			byte[] bytes = System.IO.File.ReadAllBytes(fullPath);
			Facebook.instance.postImage(bytes, Language.Get("FACEBOOK_PUBLICATION_TEXT")  , completionHandler);
			infoManager.SetMessage("PUBLISHED_ON_FACEBOOK", image);
			
		}catch(Exception){		
			infoManager.SetMessage("NOT_PUBLISHED_ON_FACEBOOK", null);
		}finally{		
			File.Delete (fullPath);
		}
		
		infoManager.ToogleMessage();
		
	}
	
	public void CancelPublication(){        
		File.Delete(fullPath);
		
		infoManager.ToogleQuestion();
	}   

#endif

    
}
