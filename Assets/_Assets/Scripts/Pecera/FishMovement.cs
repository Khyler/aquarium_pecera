﻿using UnityEngine;
using System.Collections;

public class FishMovement : MonoBehaviour
{


    private const int MIN_X = -750;
    private const int MAX_X = 750;
    private const int MIN_Y = -50;
    private const int MAX_Y = 400;
    private const int MIN_Z = -500;
    private const int MAX_Z = 500;

    private const int X_DEPTH = 100;
    private const int Y_DEPTH = 100;

    public Transform targetEndPoint;
    public Transform target;
    public float changeTargetMinDist;
    public float speed;

    private bool leftDirection = true;
	private bool dead = false;

	private Animation animation;

    void Start()
    {
		animation = GetComponent<Animation>();

        
		//TargetEndPoint location
		setNewRandomPosition(targetEndPoint);
        //Target location
        setNewRandomPosition(target);
		//Fish location
		transform.position=target.position;
      

    }

    void Update(){ 
		if (!dead) {
			CheckSwim ();
		}else{
			CheckDeath();
		}
		MoveTowardsTarget ();
	}

	public void Die(){
		dead = true;
		targetEndPoint.localPosition = new Vector3(target.localPosition.x, MAX_Y+150, target.localPosition.z);
		//MoveTowardsTarget ();		
		GetComponent<Animation>().Rewind ();
		GetComponent<Animation>().Stop();
		GetComponent<Animation>().enabled=false;
		StartCoroutine (DieInTime());
	}

	private IEnumerator DieInTime(){
		yield return new WaitForSeconds (10);
		Destroy (transform.parent.gameObject);	
	}

	private void CheckDeath(){
		if (transform.localPosition.y > MAX_Y+100) {
			GameObject.Destroy(transform.parent.gameObject);
		}
	}

	private void CheckSwim(){
		//Check target has arrived end point and it is close enough

		if (targetEndPoint.transform.localPosition == target.transform.localPosition &&
			Vector3.Distance (targetEndPoint.transform.localPosition, transform.localPosition) < changeTargetMinDist) {
			setNewRandomPosition (targetEndPoint);
		}
		       
		//Check movement direction and animations

		if (hasDirectionChanged ()) {
			leftDirection = !leftDirection;

			if (leftDirection) {
				animation.Play ("Turn left");
				animation.PlayQueued ("Swim left");
			} else {
				animation.Play ("Turn right");
				animation.PlayQueued ("Swim right");
			}
		}

		/*
        //Check rotation
                
        float rot = 0;        
        float a = target.transform.position.y - transform.position.y;
        float b = target.transform.position.x - transform.position.x;
        rot = Mathf.Atan2(a, b) * Mathf.Rad2Deg;
        
        transform.eulerAngles = new Vector3(0, 0, rot);
        */
	}

	private void MoveTowardsTarget(){
		//Move target Towards targetEndPoint		
		float targetStep = speed * Time.deltaTime;
		target.transform.position = Vector3.MoveTowards(target.transform.position, targetEndPoint.position, targetStep);

        //Lerp towards target
        float fishStep = speed * 2f * Time.deltaTime;
        transform.position = Vector3.Lerp(transform.position, target.position, fishStep);		      
    }

    void OnTriggerEnter(Collider col)
    {
        //Debug.Log("Fish:trigger enter");
        if (col.gameObject.tag == "Obstacle")
        {

            Vector3 point;
            int zChange;
            if (transform.localPosition.z > col.transform.localPosition.z)
            {
                zChange = 100;
            }
            else
            {
                zChange = -100;
            }

            point = target.localPosition;
            point.z = col.transform.localPosition.z + zChange;
            target.localPosition = point;

            point = targetEndPoint.localPosition;
            point.z = col.transform.localPosition.z + zChange;
            targetEndPoint.localPosition = point;

        }
    }

    private void setNewRandomPosition(Transform t)
    {
        Vector3 point = new Vector3();
        point.z = Random.Range(MIN_Z, MAX_Z);
        float zDepthPercentage = -(point.z - MAX_Z) / (MAX_Z - MIN_Z);
        point.y = Random.Range(MIN_Y - (zDepthPercentage * Y_DEPTH), MAX_Y);
        point.x = Random.Range(MIN_X - (zDepthPercentage * X_DEPTH), MAX_X + (zDepthPercentage * X_DEPTH));
        t.transform.localPosition = point;

        //t.transform.localPosition = new Vector3(Random.Range(MIN_X, MAX_X), Random.Range(MIN_Y, MAX_Y), Random.Range(MIN_Z, MAX_Z));
    }

    private bool hasDirectionChanged()
    {
        return (transform.position.x > target.transform.position.x) != leftDirection;
    }
}
