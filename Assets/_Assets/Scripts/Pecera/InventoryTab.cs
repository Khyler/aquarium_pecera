﻿using UnityEngine;
using System.Collections;

public class InventoryTab : MonoBehaviour {

	public InventoryManager inventoryManager { get; set; }
	
	public int index{ get; set; }
	
	private UIButton button;
	
	public void SetEnabled(bool b){
		GetComponent<UIButton>().isEnabled = b;
	}
	
	
	public void SelectTab(){
		inventoryManager.ChangeTab (index);
	}
}
