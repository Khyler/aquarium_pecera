﻿using UnityEngine;
using System.Collections;

public class CameraRestriction : MonoBehaviour {

	private Camera camera;

	public float maxX = 410;
	public float minX = -410;
	
	public float maxY= 240;
	public float minY = -90;
	
	
	
	//private float aspectRatio;

	// Use this for initialization
	void Start () {
		//aspectRatio = Screen.width / Screen.height;
		camera = GetComponent<Camera> ();
	}
	
	// Update is called once per frame
	public void CorrectPosition () {
		float factor=2*(1 - camera.orthographicSize);
		
		float x= transform.localPosition.x;
		x= Mathf.Max(minX*factor, x);
		x= Mathf.Min(maxX*factor, x);
		
		
		float y= transform.localPosition.y;
		y= Mathf.Max(minY*factor, y);
		y= Mathf.Min(maxY*factor, y);
		
		transform.localPosition=new Vector3(x,y,0);
	}
}
