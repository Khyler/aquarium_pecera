using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class InventoryManager:MonoBehaviour, ITabable
{
	public Camera movingCamera;

	public UIPanel controlsPanel;

	public UIPanel inventoryPanel;

	public UIGrid inventoryItemsGrid;
	public UIGrid inventoryTabsGrid;
	public UISprite inventorySprite;

	public UIPanel itemPanel;
	public GameObject aquariumItemParent;

	public GameObject inventoryItemPrefab;
	public GameObject aquariumItemPrefab;
	public GameObject inventoryTabPrefab;

	private List<ItemModel> aquariumItems = new List<ItemModel> ();

	private List<Tab> inventoryTabs = new List<Tab>();
	private List<InventoryItemModel> inventoryItems = new List<InventoryItemModel>();

	private List<ItemClass> itemTypes = new List<ItemClass> ();
	private int index =0;
	private const int ITEMS_PER_TAB = 4;

	private ShopManager shopManager;	
	private SaveManager saveManager;

	public bool isEnabled{ get; set; }


	void Start(){	
		isEnabled = false;
		saveManager = GetComponent<SaveManager> ();
		shopManager = GetComponent<ShopManager> ();
		inventoryPanel.alpha=0;
		UpdateItemModels ();
	}

	public void UpdateItemModels(){

		foreach(ItemModel itemModel in aquariumItems){
			Destroy (itemModel.gameObject);
		}
		aquariumItems = new List<ItemModel> ();

		foreach (ItemClass itemClass in Data.aquarium.inventory.itemClasses) {
			foreach(Item item in itemClass.items){
				if(item.onAquarium){
					aquariumItems.Add(InstantiateItemModelToAquarium(item));
				}
				
			}
		}
		UpdateTabs ();
	}

	public void ToogleInventory(){
		float alpha = inventoryPanel.alpha;
		inventoryPanel.alpha = 1 - alpha;
		controlsPanel.alpha = alpha;  

		isEnabled = (alpha==0);
		movingCamera.GetComponent<OrthoCameraMove>().enabled=(alpha==1);

	}

	public void MarkInventory(){
		inventorySprite.color = Color.cyan;
	}

	public void UnmarkInventory(){
		inventorySprite.color = Color.white;
	}

	public void ChangeTab(int index){
		inventoryTabs [this.index].SetEnabled (true);
		this.index = index;
		inventoryTabs [this.index].SetEnabled (false);
		UpdateTabs ();
	}
	
	public void NextTab(){

		ChangeTab((index+1)%inventoryTabs.Count);

	}
	public void PreviousTab(){

		ChangeTab((index+inventoryTabs.Count-1)%inventoryTabs.Count);

	}


	public void UpdateInventory(){
		foreach (InventoryItemModel item in inventoryItems) {
			item.UpdateItem();
		}

		shopManager.UpdateShop ();
	}

	public void UpdateTabs(){
		itemTypes = new List<ItemClass> ();;
		foreach (ItemClass itemClass in Data.aquarium.inventory.itemClasses) {
			if (itemClass.items.Count > 0) {
				itemTypes.Add (itemClass);
			}
		}

		foreach (InventoryItemModel inventoryItem in inventoryItems) {
			GameObject.DestroyImmediate(inventoryItem.gameObject);
		}

		inventoryItems = new List<InventoryItemModel>();
		for (int i = index*ITEMS_PER_TAB; i < (index +1) *ITEMS_PER_TAB; i++){
			if (i < itemTypes.Count){
				InventoryItemModel inventoryItem = InstantiateInventoryItemModel(itemTypes[i]);
				inventoryItems.Add(inventoryItem);
				inventoryItem.UpdateItem();
			}
		}
		inventoryItemsGrid.Reposition ();

		foreach (Tab inventoryTab in inventoryTabs) {
			GameObject.DestroyImmediate(inventoryTab.gameObject);
		}
		inventoryTabs = new List<Tab> ();
		for (int i = 0; i <= (itemTypes.Count-1)/ITEMS_PER_TAB; i++) {
			inventoryTabs.Add(InstantiateInventoryTab(i));		
		}
		inventoryTabs [index].SetEnabled (false);
		inventoryTabsGrid.Reposition ();
	}

	public void MoveInAquarium(ItemModel itemModel, Vector3 newPosition){
		itemModel.transform.localPosition= newPosition;
		itemModel.itemData.x=newPosition.x;	
		itemModel.itemData.y=newPosition.y;	
		itemModel.itemData.z=newPosition.z;	
		saveManager.Save ();
	}

	public void AddToAquarium(ItemModel itemModel){
		foreach (ItemClass i in Data.aquarium.inventory.itemClasses) {
			if (i.type==itemModel.itemData.type){
				itemModel.itemData.onAquarium = true;
				itemModel.itemData.x = itemModel.transform.localPosition.x;
				itemModel.itemData.y = itemModel.transform.localPosition.y;
				itemModel.itemData.z = itemModel.transform.localPosition.z;
				itemModel.transform.parent = aquariumItemParent.transform;
				itemModel.GetComponent<BoxCollider> ().enabled = true;			
				itemModel.alpha = 1f;
				aquariumItems.Add (itemModel);
				i.onInventory--;
				break;
			}
		}
		UpdateInventory ();		
		saveManager.Save ();
	}

	public void AddToInventory(ItemModel itemModel){
		foreach (Item i in Data.aquarium.inventory.itemClasses[(int)itemModel.itemData.type].items) {
			if (i == itemModel.itemData) {
				itemModel.itemData.onAquarium = false;
				Data.aquarium.inventory.itemClasses[(int)itemModel.itemData.type].onInventory++;
				aquariumItems.Remove(itemModel);
				break;
			}
		}
		UpdateInventory ();
		saveManager.Save ();
	}

	public ItemModel GetItemModel(ItemClass itemType){
		Item item = null;
		foreach (Item i in itemType.items) {
			if (!i.onAquarium) {
				item = i;
				break;
			}
		}
		if (item == null) {
			return null;
		}			
		return InstantiateGhostItemModel(item);
	}

	private InventoryItemModel InstantiateInventoryItemModel(ItemClass itemType){
		GameObject obj = Instantiate (inventoryItemPrefab);
		obj.transform.parent = inventoryItemsGrid.transform;		
		obj.transform.localPosition = Vector3.zero;
		obj.transform.localEulerAngles = Vector3.zero;
		obj.transform.localScale = Vector3.one;

		InventoryItemModel inventoryItemModel = obj.GetComponent<InventoryItemModel> ();
		//shopItemModel.aquariumItemParent = aquariumItemParent;
		//shopItemModel.itemParent = itemPanel.gameObject;
		inventoryItemModel.itemType = itemType;				
		//shopItemModel.shopSprite = inventorySprite;
		inventoryItemModel.inventoryManager = this;

		return inventoryItemModel;
	}

	private ItemModel InstantiateItemModelToAquarium(Item item){
		GameObject obj = Instantiate (aquariumItemPrefab);
		obj.transform.parent = aquariumItemParent.transform;		
		obj.transform.localPosition =  new Vector3 (item.x, item.y, item.z);
		obj.transform.localEulerAngles = Vector3.zero;
		obj.transform.localScale = Vector3.one;
		
		ItemModel itemModel = obj.GetComponent<ItemModel> ();
		itemModel.itemData = item;
		itemModel.inventoryManager = this;
		
		return itemModel;
	}

	public ItemModel InstantiateGhostItemModel(Item item){
		GameObject obj = Instantiate (aquariumItemPrefab);
		obj.transform.parent = aquariumItemParent.transform;		
		obj.transform.localPosition = new Vector3 (item.x, item.y, item.z);
		obj.transform.localEulerAngles = Vector3.zero;
		obj.transform.localScale = Vector3.one;

		obj.GetComponent<BoxCollider> ().enabled = false;

		ItemModel itemModel = obj.GetComponent<ItemModel> ();
		itemModel.itemData = item;
		itemModel.inventoryManager = this;
		itemModel.alpha = 0.5f;


		/*itemModel.shopSprite = inventorySprite;
		itemModel.itemParent = itemPanel.gameObject;*/



		return itemModel;
	}

	
	private Tab InstantiateInventoryTab(int index){
		GameObject obj = Instantiate (inventoryTabPrefab);
		obj.transform.parent = inventoryTabsGrid.transform;		
		obj.transform.localPosition = Vector3.zero;
		obj.transform.localEulerAngles = Vector3.zero;
		obj.transform.localScale = Vector3.one;
		
		Tab inventoryTab = obj.GetComponent<Tab> ();
		inventoryTab.index = index;
		inventoryTab.tabedPanel = this;
		
		return inventoryTab;
	}
}

