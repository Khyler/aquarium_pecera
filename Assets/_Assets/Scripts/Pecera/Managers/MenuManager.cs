﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class MenuManager : MonoBehaviour, ITabable{

	public UIPanel menuPanel;
	public UIPanel controlsPanel;
	public UIPanel helpPanel;

	public UILabel helpLabel;
	public UILabel tittleLabel;

	public InfoManager infoManager;
	public GameObject helpTabPrefab;

	public UIGrid helpTabsGrid;

	private int helpTexts=3;

	private int tabIndex=0;

	private List<Tab> helpTabs = new List<Tab> ();

	void Start(){
		menuPanel.alpha = 0;
		helpPanel.alpha = 0;

		for (int i=0; i< helpTexts; i++) {
			helpTabs.Add(InstantiateHelpTab(i));
		}
		helpTabs [tabIndex].SetEnabled (false);
		helpTabsGrid.Reposition ();
		UpdateHelp ();
	}

	private Tab InstantiateHelpTab(int index){
		GameObject obj = Instantiate (helpTabPrefab);
		obj.transform.parent = helpTabsGrid.transform;		
		obj.transform.localPosition = Vector3.zero;
		obj.transform.localEulerAngles = Vector3.zero;
		obj.transform.localScale = Vector3.one;
		
		Tab helpTab = obj.GetComponent<Tab> ();
		helpTab.index = index;
		helpTab.tabedPanel = this;
		
		return helpTab;
	}


	public void ChangeTab(int index){

		helpTabs [tabIndex].SetEnabled (true);
		tabIndex = index;
		helpTabs [tabIndex].SetEnabled (false);

		UpdateHelp ();
	}
	
	public void NextTab(){			
		ChangeTab ((tabIndex + 1)%helpTexts);
	}
	
	public void PreviousTab(){
		ChangeTab ((tabIndex +helpTexts- 1)%helpTexts);

	}

	private void UpdateHelp(){
		string helpkey = "HELP_TEXT_" + tabIndex;
		helpLabel.text = Language.Get (helpkey);
		helpLabel.GetComponent<TextLocalization> ().Key = helpkey;
		string tittleKey = "HELP_TITTLE_" + tabIndex;
		tittleLabel.text = Language.Get (tittleKey);
		tittleLabel.GetComponent<TextLocalization> ().Key = tittleKey;
	}


	public void ToogleMenu(){
		float alpha = menuPanel.alpha;
		menuPanel.alpha = 1 - alpha;
		controlsPanel.alpha = alpha;       
	}


	public void ToogleHelp(){
		float alpha = helpPanel.alpha;
		helpPanel.alpha = 1 - alpha;
	}
	


	
	public void ResetGame(){
		ToogleMenu ();
		infoManager.SetQuestion("RESET_GAME?", null, new EventDelegate(this, "AcceptResetGame"),  new EventDelegate(this, "CancelResetGame"));
		infoManager.ToogleQuestion ();
	}
	public void AcceptResetGame(){
		Data.aquarium = new Aquarium ();
		GetComponent<FishManager> ().InstantiateFishModels ();
		GetComponent<ShopManager> ().UpdateShop ();
		GetComponent<WaterManager> ().WaterUpdate (true);
		GetComponent<InventoryManager> ().UpdateItemModels ();
		GetComponent<SimulationManager> ().Reset ();
		infoManager.ToogleQuestion ();
	}
	
	public void CancelResetGame(){
		infoManager.ToogleQuestion ();
	}

}
