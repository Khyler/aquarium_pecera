using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class ShopManager:MonoBehaviour , ITabable
{

	public Camera movingCamera;

    public UIPanel controlsPanel;

	public UIPanel shopPanel;
	public UIGrid shopGrid;
	public UIGrid shopTabsGrid;

	public GameObject shopItemPrefab;
	public GameObject shopFishPrefab;
	public GameObject shopTabPrefab;
	
	public UILabel pointLabel;
	public UIButton itemsTabButton;
	public UIButton fishTabButton;


	private List<Tab> shopTabs = new List<Tab>();
	private List<ShopItemModel> shopItems = new List<ShopItemModel>();
	private List<ShopFish> shopFishes = new List<ShopFish>();

	private List<Fish.TYPE> fishTypes= new List<Fish.TYPE>();
	private List<ItemClass> itemTypes= new List<ItemClass>();
	private int itemTabIndex =0;
	private int fishTabIndex =0;

	private const int ITEMS_PER_TAB = 6;

	private bool itemsTab = true;

	private InventoryManager inventoryManager;
	private FishManager fishManager;
	private SaveManager saveManager;


	void Start(){
		saveManager = GetComponent<SaveManager> ();
		inventoryManager = GetComponent<InventoryManager> ();
		fishManager = GetComponent<FishManager> ();
		shopPanel.alpha=0;
		itemTypes = new List<ItemClass> (Data.aquarium.inventory.itemClasses);
		fishTypes = new List<Fish.TYPE> ((Fish.TYPE[])Enum.GetValues(typeof(Fish.TYPE)));
			
		//ShowShopItems ();
		UpdateShopTab ();
	}

	public void ChangeTab(int index){
		if (itemsTab) {
			shopTabs [this.itemTabIndex].SetEnabled (true);
			this.itemTabIndex = index;
			shopTabs [this.itemTabIndex].SetEnabled (false);
		} else {
			shopTabs [this.fishTabIndex].SetEnabled (true);
			this.fishTabIndex = index;
			shopTabs [this.fishTabIndex].SetEnabled (false);
		}
		//ShowShopItems ();
		UpdateShopTab ();
	}

	public void NextTab(){
		if (itemsTab) {

			ChangeTab ((itemTabIndex + 1)%shopTabs.Count);

		} else {
			ChangeTab ((fishTabIndex + 1)%shopTabs.Count);
		}
	}

	public void PreviousTab(){
		if (itemsTab) {
			ChangeTab ((itemTabIndex +shopTabs.Count- 1)%shopTabs.Count);
		} else {
			ChangeTab ((fishTabIndex +shopTabs.Count- 1)%shopTabs.Count);
		}
	}

	public void SetItemsTab(){
		itemsTab = true;
		UpdateShopTab ();
	}

	public void SetFishTab(){
		itemsTab = false;
		UpdateShopTab ();		
	}

	public void Sell(Fish.TYPE fishType){
		int count = 0;
		foreach (Fish fish in Data.aquarium.fishes.adultFishes) {
			if (fish.type== fishType){
				count++;
			}
		}
		foreach (Fish fish in Data.aquarium.fishes.childFishes) {
			if (fish.type== fishType){
				count++;
			}
		}
		if (count>0) {
			Data.aquarium.points+=Constants.fishPrice[(int)fishType];
			fishManager.RemoveFish(fishType);
		}
		UpdateShop ();

		saveManager.Save ();
	}

	public void Buy(Fish.TYPE fishType){
		if (Data.aquarium.points >= Constants.fishPrice[(int)fishType]) {
			Data.aquarium.points-=Constants.fishPrice[(int)fishType];
			fishManager.NewFish(fishType);
		}
		UpdateShop ();
		saveManager.Save ();
	}

	public void Buy(ItemClass itemClass){
		if (Data.aquarium.points >= Constants.itemPrice[(int)itemClass.type]) {
			Data.aquarium.points-=Constants.itemPrice[(int)itemClass.type];
			itemClass.items.Add(new Item(itemClass));
			itemClass.onInventory++;
		}
		UpdateShop ();		
		inventoryManager.UpdateTabs ();
		saveManager.Save ();
	}

	public void Sell(ItemClass itemClass){
		if (itemClass.onInventory > 0) {
			Item item= null;
			foreach (Item i in itemClass.items){
				if(!i.onAquarium){
					item=i;
					break;
				}
			}
			itemClass.items.Remove(item);
			itemClass.onInventory--;
			Data.aquarium.points+=Constants.itemPrice[(int)itemClass.type];
		}
		UpdateShop ();		
		inventoryManager.UpdateTabs ();
		saveManager.Save ();
	}


	public void ToogleShop(){
		float alpha = shopPanel.alpha;
		shopPanel.alpha = 1 - alpha;
		controlsPanel.alpha = alpha;  
	}

	public void UpdateShop(){
		foreach (ShopItemModel shopItem in shopItems) {
			shopItem.UpdateShopItem();
		}
		foreach (ShopFish shopFish in shopFishes) {
			shopFish.UpdateShopFish();
		}
		
		pointLabel.text = Data.aquarium.points.ToString ("0");
	}

	public void UpdateShopTab(){
		itemsTabButton.isEnabled = !itemsTab;
		fishTabButton.isEnabled = itemsTab;

		foreach (ShopItemModel shopItem in shopItems) {
			GameObject.DestroyImmediate(shopItem.gameObject);
		}
		shopItems = new List<ShopItemModel>();
		foreach (ShopFish shopFish in shopFishes) {
			GameObject.DestroyImmediate(shopFish.gameObject);
		}
		shopFishes = new List<ShopFish>();

		if (itemsTab) {
			for (int i = itemTabIndex*ITEMS_PER_TAB; i < (itemTabIndex +1) *ITEMS_PER_TAB; i++) {
				if (i < itemTypes.Count) {
					ShopItemModel shopItem=InstantiateShopItem (itemTypes [i]);
					shopItems.Add (shopItem);
					shopItem.UpdateShopItem();
				}
			}
		} else {
			for (int i = fishTabIndex*ITEMS_PER_TAB; i < (fishTabIndex +1) *ITEMS_PER_TAB; i++) {
				if (i < fishTypes.Count) {
					ShopFish shopFish = InstantiateShopFish (fishTypes [i]);
					shopFishes.Add (shopFish);
					shopFish.UpdateShopFish();
				}
			}
		}
		shopGrid.Reposition ();

		foreach (Tab shopTab in shopTabs) {
			GameObject.DestroyImmediate(shopTab.gameObject);
		}
		shopTabs = new List<Tab> ();

		if (itemsTab) {
			for (int i = 0; i <= (itemTypes.Count-1)/ITEMS_PER_TAB; i++) {
				shopTabs.Add (InstantiateShopTab (i));		
			}
			shopTabs [itemTabIndex].SetEnabled (false);
		} else {
			for (int i = 0; i <= (fishTypes.Count-1)/ITEMS_PER_TAB; i++) {
				shopTabs.Add (InstantiateShopTab (i));		
			}
			shopTabs [fishTabIndex].SetEnabled (false);
		}
		shopTabsGrid.Reposition ();
		
		pointLabel.text = Data.aquarium.points.ToString ("0");
	}
	/*
	private void ShowShopItems(){
		foreach (ShopItemModel shopItem in shopItems) {
			GameObject.DestroyImmediate(shopItem.gameObject);
		}
		shopItems = new List<ShopItemModel>();
		for (int i = itemTabIndex*ITEMS_PER_TAB; i < (itemTabIndex +1) *ITEMS_PER_TAB; i++){
			if (i < itemTypes.Count){
				shopItems.Add(InstantiateShopItem(itemTypes[i]));
			}
		}
		shopGrid.Reposition ();
	}*/

	private ShopItemModel InstantiateShopItem(ItemClass itemType){
		GameObject obj = Instantiate (shopItemPrefab);
		obj.transform.parent = shopGrid.transform;		
		obj.transform.localPosition = Vector3.zero;
		obj.transform.localEulerAngles = Vector3.zero;
		obj.transform.localScale = Vector3.one;

		ShopItemModel shopItemModel = obj.GetComponent<ShopItemModel> ();
		shopItemModel.itemClass = itemType;
		shopItemModel.shopManager = this;

		return shopItemModel;
	}

	private ShopFish InstantiateShopFish(Fish.TYPE fishType){
		GameObject obj = Instantiate (shopFishPrefab);
		obj.transform.parent = shopGrid.transform;		
		obj.transform.localPosition = Vector3.zero;
		obj.transform.localEulerAngles = Vector3.zero;
		obj.transform.localScale = Vector3.one;
		
		ShopFish shopFish = obj.GetComponent<ShopFish> ();
		shopFish.fishType = fishType;
		shopFish.shopManager = this;
		
		return shopFish;
	
	}

	private Tab InstantiateShopTab(int index){
		GameObject obj = Instantiate (shopTabPrefab);
		obj.transform.parent = shopTabsGrid.transform;		
		obj.transform.localPosition = Vector3.zero;
		obj.transform.localEulerAngles = Vector3.zero;
		obj.transform.localScale = Vector3.one;

		Tab shopTab = obj.GetComponent<Tab> ();
		shopTab.index = index;
		shopTab.tabedPanel = this;
		
		return shopTab;
	}
}

