﻿using UnityEngine;
using System.Collections;

public class InfoManager : MonoBehaviour {

    public UIPanel infoPanel;
    public UIWidget messageWidget;
	public UIWidget questionWidget;

    public UIPanel controlsPanel;

    public UILabel messageLabel;
    public UILabel questionLabel;
	public UITexture messageImage;
	public UITexture questionImage;
    public UIButton yesButton;
    public UIButton noButton;
	
	void Start () {
		infoPanel.alpha = 0;
	}

    public void ToogleMessage() {
		float alpha = infoPanel.alpha;
		infoPanel.alpha = 1 - alpha;
		controlsPanel.alpha = alpha;  
		messageWidget.alpha = 1 - alpha;
		questionWidget.alpha = alpha;       
    }

    public void SetMessage(string messageKey, Texture image) {
		messageLabel.gameObject.GetComponent<TextLocalization> ().Key = messageKey;
		messageLabel.text = Language.Get(messageKey);
		if (image == null) {
			messageWidget.height = 400;
		} else {
			messageWidget.height = 850;	
		}
		messageImage.mainTexture = image;
    }

    public void ToogleQuestion() {
		float alpha = infoPanel.alpha;
		infoPanel.alpha = 1 - alpha;
		controlsPanel.alpha = alpha;  
		questionWidget.alpha = 1 - alpha;
		messageWidget.alpha = alpha;   
    }

	public void SetQuestion(string questionKey, Texture image, EventDelegate yesAction, EventDelegate noAction)
    {
		questionLabel.gameObject.GetComponent<TextLocalization> ().Key = questionKey;
		questionLabel.text = Language.Get(questionKey);
        
        yesButton.onClick[0]=yesAction;
        noButton.onClick[0] = noAction;

		if (image == null) {
			questionWidget.height = 400;
		} else {
			questionWidget.height = 850;	
		}
		questionImage.mainTexture = image;
    }
}
