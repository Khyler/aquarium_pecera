﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class SimulationManager : MonoBehaviour {


	public UILabel label;

	private FishManager fishManager;
	private WaterManager waterManager;
	private SaveManager saveManager;
	private DateTime lastSimulation;
	private Simulation simulation;

	private readonly float SIMULATION_TIME = 60*60*24;


	void Start(){
		fishManager = GetComponent<FishManager> ();
		waterManager = GetComponent<WaterManager> ();
		saveManager = GetComponent<SaveManager> ();
		SimulatePreviousAll ();
		DateTime now = DateTime.Now;
		float nextSim = SIMULATION_TIME - (float)((now.Ticks / 10000000) - Data.aquarium.lastSim);
		Debug.Log ( "LastSim: " + new DateTime(Data.aquarium.lastSim*10000000).ToString ("dd-MM-yyyy hh:mm:ss")+ " -- NextSim: "+ new DateTime ((Data.aquarium.lastSim + (long)SIMULATION_TIME) * 10000000).ToString ("dd-MM-yyyy hh:mm:ss"));
		//label.text=( "LastSim: " + new DateTime(Data.aquarium.lastSim*10000000).ToString ("dd-MM-yyyy hh:mm:ss")+ " -- NextSim: "+ new DateTime ((Data.aquarium.lastSim + (long)SIMULATION_TIME) * 10000000).ToString ("dd-MM-yyyy hh:mm:ss"));
		InvokeRepeating ("Simulate", nextSim, SIMULATION_TIME);
	}

	public void Reset(){
		CancelInvoke ("Simulate");
		InvokeRepeating ("Simulate", SIMULATION_TIME, SIMULATION_TIME);
		saveManager.Save ();
	}

	private void Simulate(){
		Simulation simulation = new Simulation ();
		SimulationResult result = simulation.GetResult ();	
		ApplySimulationResult (result);
		fishManager.UpdateFishData (true);
		waterManager.WaterUpdate (false);
		saveManager.Save ();
	}

	void OnApplicationFocus( bool focusSatus){
		if (focusSatus) {
			SimulatePreviousAll();
		}		
	}

	private void SimulatePreviousAll(){
		fishManager = GetComponent<FishManager> ();
		waterManager = GetComponent<WaterManager> ();
		while((DateTime.Now.Ticks/10000000)>=(Data.aquarium.lastSim+ (long)SIMULATION_TIME)){
			Simulation simulation = new Simulation ();
			SimulationResult result = simulation.GetResult ();		
			ApplySimulationResult (result);
			fishManager.UpdateFishData (false);
		}
		fishManager.InstantiateFishModels();
		waterManager.WaterUpdate (true);
		GetComponent<SaveManager>().Save ();	
	}

	private void ApplySimulationResult(SimulationResult result){
		
		Debug.Log ("Apply simulation");


		Data.aquarium.lastSim += (long)SIMULATION_TIME;

		Data.aquarium.points = result.newPoints;

		Data.aquarium.water.foodLevel = result.newFoodLevel;
		Data.aquarium.water.poopLevel = result.newPoopLevel;
		
		Data.aquarium.fishes.deadFishes = result.newDeadFishes;
		Data.aquarium.fishes.grownFishes = result.newGrownFishes;
		Data.aquarium.fishes.bornFishes = result.newBornFishes;

		Data.aquarium.water.foodPills = result.newFoodPills;
		Data.aquarium.water.poopPills = result.newPoopPills;


		Debug.Log ( "LastSim: " + new DateTime(Data.aquarium.lastSim*10000000).ToString ("dd-MM-yyyy hh:mm:ss")+ " -- NextSim: "+ new DateTime ((Data.aquarium.lastSim + (long)SIMULATION_TIME) * 10000000).ToString ("dd-MM-yyyy hh:mm:ss"));
		//label.text=( "LastSim: " + new DateTime(Data.aquarium.lastSim*10000000).ToString ("dd-MM-yyyy hh:mm:ss")+ " -- NextSim: "+ new DateTime ((Data.aquarium.lastSim + (long)SIMULATION_TIME) * 10000000).ToString ("dd-MM-yyyy hh:mm:ss"));

	}



}
