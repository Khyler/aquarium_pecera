﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WaterManager: MonoBehaviour
{





	public UILabel PoopLevelLabel;
	public UILabel FoodLevelLabel;
	public UILabel PoopPillsLabel;
	public UILabel FoodPillsLabel;

	public UILabel PointsLabel;

	public UITexture DirtTexture;

	private float actualDirtLevel=0;
	private bool dirtLevelChanged=false;

	private SaveManager saveManager;

	void Start(){
		saveManager = GetComponent<SaveManager> ();
		actualDirtLevel = Data.aquarium.water.poopLevel;
		WaterUpdate (true);
	}

	void Update(){
		if (dirtLevelChanged) {
			actualDirtLevel=Mathf.Lerp(actualDirtLevel,Data.aquarium.water.poopLevel,Time.deltaTime/10);			
			DirtTexture.alpha = Mathf.Min (1f, actualDirtLevel / 480);

			if(actualDirtLevel==Data.aquarium.water.poopLevel){
				dirtLevelChanged=false;
			}
		}
	}

	public void WaterUpdate(bool changeDirtTexture){
		if (changeDirtTexture) {
			actualDirtLevel = Data.aquarium.water.poopLevel;
			DirtTexture.alpha = Mathf.Min (1, Data.aquarium.water.poopLevel / 480);
			dirtLevelChanged = false;
		} else {
			dirtLevelChanged = true;
		}
		DirtTexture.alpha = Mathf.Min (1f, actualDirtLevel / 480);
		PoopLevelLabel.text = Mathf.Min (1, Data.aquarium.water.poopLevel / 480).ToString ("0%");
		FoodLevelLabel.text = Data.aquarium.water.foodLevel.ToString ("0");
		PoopPillsLabel.text = Data.aquarium.water.poopPills.ToString ("0");
		FoodPillsLabel.text = Data.aquarium.water.foodPills.ToString ("0");

		PointsLabel.text = Data.aquarium.points.ToString ("0");
	}

	public void MorePoopPills(){
		if (Water.POOPPILLS_INCREMENT * Water.POOPPILLS_COST <= Data.aquarium.points) {
			Data.aquarium.water.poopPills += Water.POOPPILLS_INCREMENT;
			Data.aquarium.points-= (Water.POOPPILLS_INCREMENT * Water.POOPPILLS_COST);
			saveManager.Save();
			WaterUpdate (false);
		}

	}
	public void LessPoopPills(){
		if (Data.aquarium.water.poopPills > 0) {
			Data.aquarium.water.poopPills -= Water.POOPPILLS_INCREMENT;
			Data.aquarium.points += (Water.POOPPILLS_INCREMENT * Water.POOPPILLS_COST);
			saveManager.Save();
			WaterUpdate (false);
		}
	}
	public void MoreFoodPills(){
		if (Water.FOODPILLS_INCREMENT * Water.FOODPILLS_COST <= Data.aquarium.points) {
			Data.aquarium.water.foodPills += Water.FOODPILLS_INCREMENT;
			Data.aquarium.points -= (Water.FOODPILLS_INCREMENT * Water.FOODPILLS_COST);
			saveManager.Save();
			WaterUpdate (false);
		}
	}
	public void LessFoodPills(){
		if (Data.aquarium.water.foodPills > 0) {
			Data.aquarium.water.foodPills -= Water.FOODPILLS_INCREMENT;			
			Data.aquarium.points += (Water.FOODPILLS_INCREMENT * Water.FOODPILLS_COST);
			saveManager.Save();
			WaterUpdate (false);
		}
	}


}

