﻿using UnityEngine;
using System.Collections;
using Prime31;

public class LoadManager : MonoBehaviour {

	public GameObject panel;

	// Use this for initialization
	public void Load () {
		panel.SetActive(true);

		StartCoroutine ("LoadLevel");
	}

	private IEnumerator LoadLevel(){
		yield return new WaitForSeconds(0.3f);

		if(PlayerPrefs.HasKey("AquariumData")){
			Aquarium obj = Json.decode<Aquarium>(PlayerPrefs.GetString("AquariumData"),null);
			Data.aquarium = obj;
			Debug.Log ("Game loaded");
		}
		Application.LoadLevel (Application.loadedLevel + 1);
	}

}
