using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FishManager: MonoBehaviour
{
	public readonly int[] fishVariants= new int[] {
		1,
		1,
		1,
		1,
		1,
		1,
		1,
	};

	public GameObject[] FishModels;
	public GameObject FishParent;

	private List<FishModel> ChildFishes= new List<FishModel> ();
	private List<FishModel> AdultFishes = new List<FishModel> ();


	public UILabel AdultFishLabel;
	public UILabel ChildFishLabel;

	void Start ()
	{
		//FishModelsUpdate ();
	}


	public void UpdateFishData(bool updateModels){

		//Born fishes
		List<int> types = new List<int> ();
		for (int i = 0; i < Data.aquarium.fishes.bornFishes.Length; i++) {
			if(Data.aquarium.fishes.bornFishes[i]>=1){
				types.Add (i);
			}
		}
		while (types.Count>0) {
			int type = types [Random.Range (0, types.Count)];
			int variant = Random.Range (0, fishVariants [type]);
			Fish fish = new Fish ((Fish.TYPE)type, variant);
			Data.aquarium.fishes.childFishes.Add (fish);
			if (updateModels) {
				BornFish (fish);
			}
			Data.aquarium.fishes.bornFishes [type]--;
			if (Data.aquarium.fishes.bornFishes [type] < 1) {
				types.Remove (type);
			}
		}

		//Grown fishes
		while ((int)Data.aquarium.fishes.grownFishes >0) {
			Fish fish = Data.aquarium.fishes.childFishes [0];
			fish.adult = true;
			Data.aquarium.fishes.adultFishes.Add (fish);
			Data.aquarium.fishes.childFishes.RemoveAt (0);
			if (updateModels) {
				GrowFish ();
			}
			Data.aquarium.fishes.grownFishes--;
		}
		if (Data.aquarium.fishes.childFishes.Count == 0) {
			Data.aquarium.fishes.grownFishes = 0;
		}

		//Dead fishes
		while ((int)Data.aquarium.fishes.deadFishes >0) {
			if (Data.aquarium.fishes.adultFishes.Count > 0) {
				Data.aquarium.fishes.adultFishes.RemoveAt (0);
			} else if (Data.aquarium.fishes.childFishes.Count > 0) {
				Data.aquarium.fishes.childFishes.RemoveAt (0);
			}
			Data.aquarium.fishes.deadFishes--;
			if (updateModels) {
				KillFish ();
			}
		}		
		if (Data.aquarium.fishes.childFishes.Count == 0 && Data.aquarium.fishes.adultFishes.Count == 0) {
			Data.aquarium.fishes.deadFishes = 0;
		}

		GetComponent<ShopManager> ().UpdateShop ();		
					
		UpdateFishLabels ();
	}

	private void UpdateFishLabels(){
		AdultFishLabel.GetComponent<UILabel> ().text = AdultFishes.Count.ToString ("0");
		ChildFishLabel.GetComponent<UILabel> ().text = ChildFishes.Count.ToString ("0");	
	}



	public void InstantiateFishModels(){
		//Instantiate fishes
		foreach (FishModel fishModel in AdultFishes) {
			Destroy (fishModel.transform.parent.gameObject);
		}
		AdultFishes = new List<FishModel> ();
		
		foreach (FishModel fishModel in ChildFishes) {
			Destroy (fishModel.transform.parent.gameObject);
		}
		ChildFishes = new List<FishModel> ();

		foreach (Fish fish in Data.aquarium.fishes.adultFishes) {
			FishModel fishModel = InstantiateFish (fish);
			fishModel.UpdateFishModel ();
			AdultFishes.Add (fishModel);
		}
		foreach (Fish fish in Data.aquarium.fishes.childFishes) {
			FishModel fishModel = InstantiateFish (fish);
			fishModel.UpdateFishModel();
			ChildFishes.Add (fishModel);
		}


		GetComponent<ShopManager> ().UpdateShop ();		
		
		UpdateFishLabels ();
	}


	public void RemoveFish(Fish.TYPE fishType){
		FishModel fishModel = null;
		foreach(FishModel f in AdultFishes){
			if( f.fishData.type==fishType){
				fishModel=f;
				Data.aquarium.fishes.adultFishes.Remove(f.fishData);
				AdultFishes.Remove(f);
				break;
			}
		}
		if (fishModel == null) {
			foreach(FishModel f in ChildFishes){
				if( f.fishData.type==fishType){
					fishModel=f;
					Data.aquarium.fishes.childFishes.Remove(f.fishData);
					ChildFishes.Remove(f);
					break;
				}
			}		
		}
		if (fishModel != null) {
			GameObject.Destroy(fishModel.transform.parent.gameObject);
		}

		UpdateFishLabels ();
	}

	private void KillFish(){
		FishModel fishModel =null;
		if(AdultFishes.Count>0){
			fishModel =AdultFishes[0];
			AdultFishes.RemoveAt(0);
		}else if(ChildFishes.Count>0){
			fishModel = ChildFishes[0];
			ChildFishes.RemoveAt (0);
		}
		if (fishModel != null) {
			fishModel.Die();
		}
	}

	private void GrowFish(){
		if (ChildFishes.Count > 0) {
			FishModel fishModel = ChildFishes [0];
			ChildFishes.RemoveAt (0);
			AdultFishes.Add (fishModel);
			fishModel.Grow ();
		}
	}

	public void NewFish(Fish.TYPE type){
		int variant = Random.Range(0,fishVariants[(int)type]);
		Fish fish= new Fish(type,variant);
		Data.aquarium.fishes.childFishes.Add (fish);
		BornFish(fish);		
		UpdateFishLabels ();
	}


	private void BornFish(Fish fish){
		FishModel fishModel = InstantiateFish (fish);
		ChildFishes.Add (fishModel);
		fishModel.Born ();
	}

	private FishModel InstantiateFish(Fish fish){
		GameObject obj = Instantiate (FishModels [(int)fish.type]);
		obj.transform.parent = FishParent.transform;
		obj.transform.localPosition = Vector3.zero;
		obj.transform.localEulerAngles = Vector3.zero;
		obj.transform.localScale = Vector3.one;

		FishModel fishModel = obj.GetComponentInChildren<FishModel> ();
		fishModel.fishData=fish;
		return fishModel;
	}
}
