﻿using UnityEngine;
using System.Collections;

public class ShopFish : MonoBehaviour {

	public ShopManager shopManager{ get; set; }
	
	public Fish.TYPE fishType{ get; set; }
	
	public UISprite fishSprite;
	public UILabel priceLabel;
	public UILabel quantityLabel;
	
	public UIButton buyButton;
	public UIButton sellButton;

	// Use this for initialization
	void Start () {
		fishSprite.spriteName = Constants.fishSpriteNames [(int)fishType];
	}

	public void UpdateShopFish () {

		
		priceLabel.text = Constants.fishPrice[(int)fishType].ToString ("0");
		int count = 0;
		foreach (Fish fish in Data.aquarium.fishes.adultFishes) {
			if (fish.type== fishType){
				count++;
			}
		}
		foreach (Fish fish in Data.aquarium.fishes.childFishes) {
			if (fish.type== fishType){
				count++;
			}
		}
		quantityLabel.text = "x"+count.ToString ("0");

		buyButton.isEnabled = Data.aquarium.points >= Constants.fishPrice[(int)fishType];
		sellButton.isEnabled = count > 0;
	}
	
	public void Buy(){
		shopManager.Buy (fishType);
	}

	public void Sell(){
		shopManager.Sell (fishType);
	}

}
