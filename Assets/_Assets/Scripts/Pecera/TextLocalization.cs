﻿using UnityEngine;
using System.Collections;

public class TextLocalization : MonoBehaviour 
{
	public string Key;
	void Start () 
	{
		gameObject.tag= "Localizable";
		UpdateText ();
	}

	public void UpdateText(){
		GetComponent<UILabel>().text = Language.Get(Key);
	}

}
