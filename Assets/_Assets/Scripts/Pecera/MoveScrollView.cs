﻿using UnityEngine;
using System.Collections;

public class MoveScrollView : MonoBehaviour {

	public enum Direction{
		Right,
		Left,
	}


	public Direction direction;
	public UIScrollView scrollView;
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnClick(){
		Vector3 movement = new Vector3 (0, 0, 0);
		switch (direction) {
		case Direction.Left:
			movement = new Vector3 (250, 0, 0);
			break;
		case Direction.Right:
			movement = new Vector3 (-250, 0, 0);
			break;
		}

		scrollView.MoveRelative (movement);
		scrollView.RestrictWithinBounds (true);	
	}
}
