﻿using UnityEngine;
using System.Collections;

public class Seaweed : MonoBehaviour {

	private const int MAX_X = 700;
	private const int MIN_X = -700;
	private const int MAX_Y = -95;
	private const int MIN_Y = -175;
	private const int MAX_Z = 500;
	private const int MIN_Z = -500;

	
	private const int X_DEPTH = 100;

	private const int WIDTH = 50;

	// Use this for initialization
	void Start () {
		CorrectPosition ();
	}
	
	public void CorrectPosition(){
		
		Vector3 point = transform.localPosition;
		
		point.y = Mathf.Max(MIN_Y, point.y);
		point.y = Mathf.Min(MAX_Y, point.y);
		
		point.z = MIN_Z + ((point.y - MIN_Y) * (MAX_Z - MIN_Z) / (MAX_Y - MIN_Y));
		
		float zDepthPercentage = (MAX_Z - point.z) / (MAX_Z - MIN_Z);
		
		point.x = Mathf.Max(MIN_X - (zDepthPercentage * X_DEPTH) + (WIDTH / 2), point.x);
		point.x = Mathf.Min(MAX_X + (zDepthPercentage * X_DEPTH) - (WIDTH / 2), point.x);
		
		transform.localPosition = point;
	}
}
