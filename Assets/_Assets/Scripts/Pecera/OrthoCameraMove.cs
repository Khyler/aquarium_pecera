﻿using UnityEngine;
using System.Collections;

public class OrthoCameraMove: MonoBehaviour {


	public float sensitivity  = 4;
	private Vector3 lastPosition;

	private CameraRestriction cameraRestriction;

	private bool zooming=false;

	void Start(){
		cameraRestriction=GetComponent<CameraRestriction> ();
	}

	void Update()
	{
		if (Application.isMobilePlatform) {

			if (Input.touchCount > 1) {
				zooming = true;
			}
			if (Input.touchCount == 1 && Input.GetTouch (0).phase == TouchPhase.Began) {
				lastPosition = Input.GetTouch (0).position;
				zooming = false;
			}
			if (zooming) {
				lastPosition = Input.GetTouch (0).position;
			}
			
			if (Input.touchCount == 1 && Input.GetTouch (0).phase == TouchPhase.Moved && !zooming) {
				Vector3 delta = new Vector3 (Input.GetTouch (0).position.x - lastPosition.x,
				                             Input.GetTouch (0).position.y - lastPosition.y,
				                             0);
				transform.Translate (-delta.x * (sensitivity / 1000), -delta.y * (sensitivity / 1000), 0);
				lastPosition = Input.GetTouch (0).position;
				
				cameraRestriction.CorrectPosition ();
			}



		} else {

			if (Input.GetAxis ("Mouse ScrollWheel") != 0) {
				zooming = true;
			}
			if (Input.GetMouseButtonDown (0)) {
				lastPosition = Input.mousePosition;
				zooming = false;
			}
			if (zooming) {
				lastPosition = Input.mousePosition;
			}
		
			if (Input.GetMouseButton (0)) {
				Vector3 delta = new Vector3 (Input.mousePosition.x - lastPosition.x,
			                            Input.mousePosition.y - lastPosition.y,
			                            0);
				transform.Translate (-delta.x * (sensitivity / 1000), -delta.y * (sensitivity / 1000), 0);
				lastPosition = Input.mousePosition;

				cameraRestriction.CorrectPosition ();
			}

		}

			

	}
}
