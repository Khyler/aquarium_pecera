﻿using UnityEngine;
using System.Collections;
using System;
using System.IO;
using Prime31;

public class CaptureScreenShot : MonoBehaviour {

	public UIPanel controlsPanel;

	public void Capture(){
		StartCoroutine(CaptureCoroutine());
	}

	private IEnumerator CaptureCoroutine(){
		controlsPanel.alpha = 0;

		string filename = DateTime.Now.ToString ("yyMMdd_HHmmss") + ".png";


		string path = Application.persistentDataPath+"/";

		Application.CaptureScreenshot(filename);
		while (!File.Exists(path+filename) && !File.Exists(filename)) {
			yield return new WaitForSeconds(0.1f);
		}

		/*
		//Show in gallery
		if (Application.isMobilePlatform) {
			string newPath= path+"../../../../DCIM/Pecera";
			if (!Directory.Exists(newPath)){
				Directory.CreateDirectory(newPath);
			}
			File.Move(path+filename, newPath+"/"+filename);		
		}*/

		controlsPanel.alpha = 1;

	}
}
