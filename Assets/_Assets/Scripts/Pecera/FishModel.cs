﻿using UnityEngine;
using System.Collections;
using System;

public class FishModel:MonoBehaviour
{


	public Material[] materialVariants;
	
	public Fish fishData { get; set; }

	public void UpdateFishModel (){
		SetVariantMaterial (fishData.typeVariant);
		if (fishData.adult) {
			transform.localScale = new Vector3 (fishData.sizeVariant, fishData.sizeVariant, fishData.sizeVariant);
		}else{
			transform.localScale= new Vector3 (0.5f * fishData.sizeVariant, 0.5f * fishData.sizeVariant, 0.5f * fishData.sizeVariant);
		}
	}

	public void Born ()
	{
		SetVariantMaterial (fishData.typeVariant);

		TweenScale scale = GetComponent<TweenScale> ();
		scale.from= new Vector3 (0, 0, 0);
		scale.to= new Vector3 (0.5f * fishData.sizeVariant, 0.5f * fishData.sizeVariant, 0.5f * fishData.sizeVariant);
		scale.enabled = true;
		scale.PlayForward ();
	}

	public void Grow(){
		TweenScale scale = GetComponent<TweenScale> ();
		scale.from= new Vector3 (0.5f * fishData.sizeVariant, 0.5f * fishData.sizeVariant, 0.5f * fishData.sizeVariant);
		scale.to= new Vector3 (fishData.sizeVariant, fishData.sizeVariant, fishData.sizeVariant);
		scale.enabled = true;
		scale.ResetToBeginning ();
		scale.PlayForward ();
	}

	public void Die(){
		TweenRotation rotation = GetComponent<TweenRotation> ();
		rotation.enabled = true;
		rotation.PlayForward ();
		FishMovement fishMovement = GetComponent<FishMovement> ();
		fishMovement.Die ();
	}


	private int GetRandomVariant ()
	{
		return UnityEngine.Random.Range (0, materialVariants.Length);
	}


	private void SetVariantMaterial (int variant)
	{
		if (variant > materialVariants.Length) {
			foreach (Renderer r in GetComponentsInChildren<Renderer> ()) {
				r.material = materialVariants [variant];
			}
		}
	}


}
