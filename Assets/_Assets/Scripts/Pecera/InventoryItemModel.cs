using UnityEngine;
using System.Collections;

public class InventoryItemModel : MonoBehaviour {

	private const int MAX_X = 700;
	private const int MIN_X = -700;
	private const int MAX_Y = -90;
	private const int MIN_Y = -175;
	private const int MAX_Z = 500;
	private const int MIN_Z = -500;
	
	private const int X_DEPTH = 100;

	public InventoryManager inventoryManager{ get; set; }

	public ItemClass itemType{ get; set; }
	public UILabel quantityLabel;
	public UISprite itemSprite;

	private Ray ray;
	private RaycastHit hit;
	

	private ItemModel clone=null;
	
	void Start() {	
		itemSprite.spriteName = Constants.itemSpriteNames [(int)itemType.type];
		float width = Constants.itemSpriteSize [(int)itemType.type] [0];
		float height = Constants.itemSpriteSize [(int)itemType.type] [1];
		float aspectRatio = width / height;
		if (itemSprite.aspectRatio > aspectRatio) {
			itemSprite.height = 160;
			itemSprite.width = (int)(160 * aspectRatio);
		} else {
			itemSprite.width=240;
			itemSprite.height=(int)(240/aspectRatio);
		}
	}

	public void UpdateItem(){		
		quantityLabel.text = "x"+itemType.onInventory.ToString ("0");	
	}

	void OnDrag() {
		if(itemType.onInventory>0){
			if (clone == null) {
				clone= inventoryManager.GetItemModel(itemType);
			}
		
			ray = Camera.main.ScreenPointToRay (Input.mousePosition);
		
			if (Physics.Raycast (ray, out hit) && hit.collider.CompareTag ("Shop")) {				
				clone.transform.position = hit.point;
				inventoryManager.MarkInventory ();
			} else {
				clone.CorrectPosition ();
				clone.UpdateDepth ();
				inventoryManager.UnmarkInventory ();
			}
		}
	}
	
	
	void OnDragEnd ()
	{
		if (clone!= null) {

			ray = Camera.main.ScreenPointToRay (Input.mousePosition);
		
			if (Physics.Raycast (ray, out hit) && hit.collider.CompareTag ("Shop")) {
				inventoryManager.UnmarkInventory();
				GameObject.Destroy (clone.gameObject);
			} else {
				clone.CorrectPosition ();
				clone.alpha=1;
				clone.UpdateColor ();
				clone.UpdateDepth ();
				inventoryManager.AddToAquarium(clone.GetComponent<ItemModel>());	
				inventoryManager.UnmarkInventory();
			}

			clone = null;		
		}
	}

}
