using UnityEngine;
using System.Collections;
using System;

public class Simulation : MathBase {



	public Simulation(){
		waterFoodLevel = Data.aquarium.water.foodLevel;
		waterFoodPills = Data.aquarium.water.foodPills;

		waterPoopLevel = Data.aquarium.water.poopLevel;
		waterPoopPills = Data.aquarium.water.poopPills;

		adultFishes = Data.aquarium.fishes.adultFishes.ToArray();
		childFishes = Data.aquarium.fishes.childFishes.ToArray();

		bornFishes = Data.aquarium.fishes.bornFishes;
		grownFishes = Data.aquarium.fishes.grownFishes;
		deadFishes = Data.aquarium.fishes.deadFishes;

		points = Data.aquarium.points;
	}

	public SimulationResult GetResult (){

		SimulationResult result = new SimulationResult();
		result.newFoodLevel = NewWaterFoodLevel ();
		result.newPoopLevel = NewWaterPoopLevel ();	
		result.newBornFishes = NewBornFishes ();
		result.newGrownFishes = NewGrownFishes ();
		result.newDeadFishes = NewDeadFishes ();

		result.newFoodPills = NewWaterFoodPills ();
		result.newPoopPills = NewWaterPoopPills ();

		result.newPoints = NewPoints ();


		return result;

	}





}
