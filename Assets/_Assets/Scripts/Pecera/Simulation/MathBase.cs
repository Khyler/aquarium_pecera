using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class MathBase {



	protected float waterFoodLevel;
	protected float waterFoodPills;

	protected float waterPoopLevel;
	protected float waterPoopPills;

	protected Fish[] adultFishes;
	protected Fish[] childFishes;

	protected float[] bornFishes;
	protected float grownFishes;
	protected float deadFishes;

	protected float points;


	protected float NewPoints(){
		return Mathf.Max(0,points + (adultFishes.Length * 3) + (childFishes.Length * 3) - (NewWaterFoodPills() * Water.FOODPILLS_COST) - (NewWaterPoopPills() * Water.POOPPILLS_COST) - waterPoopLevel - (waterFoodLevel*0.1f));
	}

	protected float NewWaterFoodLevel(){
		return Mathf.Max (0f,(waterFoodLevel + FoodVariation()));
	}

	protected float NewWaterPoopLevel(){
		return Mathf.Max (0f, waterPoopLevel + childFishes.Length + adultFishes.Length - (waterPoopPills * 20));
	}

	protected float NewWaterFoodPills(){
		float newWaterFoodPills = 0;
		float usablePoints = points + (adultFishes.Length * 3) + (childFishes.Length * 3) - waterPoopLevel - (waterFoodLevel*0.1f);

		while (newWaterFoodPills <waterFoodPills && usablePoints >= Water.FOODPILLS_INCREMENT* Water.FOODPILLS_COST) {
			//Debug.Log (usablePoints);
			newWaterFoodPills+=Water.FOODPILLS_INCREMENT;
			usablePoints-=(Water.FOODPILLS_INCREMENT* Water.FOODPILLS_COST);
			//Debug.Log (newWaterFoodPills);
		}
		return newWaterFoodPills;
	}

	protected float NewWaterPoopPills(){
		float newWaterPoopPills = 0;
		float usablePoints = points + (adultFishes.Length * 3) + (childFishes.Length * 3)- (NewWaterFoodPills() * Water.FOODPILLS_COST) - waterPoopLevel - (waterFoodLevel*0.1f);
		while (newWaterPoopPills <waterPoopPills && usablePoints >= Water.POOPPILLS_INCREMENT* Water.POOPPILLS_COST) {
			newWaterPoopPills+=Water.POOPPILLS_INCREMENT;
			usablePoints-=(Water.POOPPILLS_INCREMENT* Water.POOPPILLS_COST);
		}
		return newWaterPoopPills;
	}


	protected float[] NewBornFishes(){

		float[] countFishes = new float[Enum.GetValues(typeof(Fish.TYPE)).Length];		
		float[] newBornFishes = new float[Enum.GetValues(typeof(Fish.TYPE)).Length];		

		foreach (Fish fish in adultFishes) {
			countFishes[(int)fish.type]++;
		}
		for (int i =0; i< countFishes.Length; i++) {
			float variation = 0;
			if(countFishes[i] >1){
				variation = countFishes[i] / 9f;
			}

			newBornFishes[i]= bornFishes[i] + variation ;

		}
		return newBornFishes;

	}

	/*
	protected float[] NewBornFishes(){
		float[] newBornFishes;
		foreach (FishModel fish in adultFishes) {
			newBornFishes[fish.fishStats.type]++;
		}
		for (int i = 0; i< newBornFishes;i++){
			if(newBornFishes[i] >1){
				newBornFishes[i]= newBornFishes[i] /9f ;
			}else{
				newBornFishes[i]=0;
			}
		}
	}
*/

	protected float NewGrownFishes(){
		return grownFishes + childFishes.Length / 3f;
	}

	protected float NewDeadFishes(){
			return  deadFishes + Mathf.Max (NewDeadByFoodLevel(), NewDeadByPoopLevel ());
	}

	private float NewDeadByPoopLevel(){
		return  (adultFishes.Length + childFishes.Length) / (1f + (1920f/(120f+ NewWaterPoopLevel()))); //0=> 1/18 , 120=> 1/9 , inf=> 1 
	}

	private float NewDeadByFoodLevel(){
			return Mathf.Max (0f, ((waterFoodLevel + FoodVariation ()) / -2f));
	}

	private float FoodVariation(){	
		return waterFoodPills - Mathf.Ceil(waterFoodLevel * 0.05f) - (childFishes.Length * 2) - (adultFishes.Length * 2);
	}




}
