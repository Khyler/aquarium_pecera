﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SimulationResult {

	public float newPoints {get; set;}

	public float newFoodLevel {get;set;}
	public float newFoodPills {get;set;}
	
	public float newPoopLevel {get;set;}
	public float newPoopPills {get;set;}

	public float[] newBornFishes {get;set;}
	public float newGrownFishes {get;set;}
	public float newDeadFishes {get;set;}
}
