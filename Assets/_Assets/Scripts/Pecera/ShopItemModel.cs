using UnityEngine;
using System.Collections;

public class ShopItemModel : MonoBehaviour {

	public ShopManager shopManager{ get; set; }
	
	public ItemClass itemClass{ get; set; }

	public UISprite itemSprite;
	public UILabel priceLabel;
	public UILabel quantityLabel;

	public UIButton buyButton;
	public UIButton sellButton;




	// Use this for initialization
	void Start () {
		itemSprite.spriteName = Constants.itemSpriteNames [(int)itemClass.type];
		float width = Constants.itemSpriteSize [(int)itemClass.type] [0];
		float height = Constants.itemSpriteSize [(int)itemClass.type] [1];
		float aspectRatio = width / height;
		if (itemSprite.aspectRatio > aspectRatio) {
			itemSprite.height = 150;
			itemSprite.width = (int)(150 * aspectRatio);
		} else {
			itemSprite.width=200;
			itemSprite.height=(int)(200/aspectRatio);
		}
	}

	public void UpdateShopItem () {
		buyButton.isEnabled = Data.aquarium.points >= Constants.itemPrice[(int)itemClass.type];
		sellButton.isEnabled = itemClass.onInventory > 0;

		priceLabel.text = Constants.itemPrice[(int)itemClass.type].ToString ("0");
		quantityLabel.text = "x"+itemClass.onInventory.ToString ("0");
	}

	public void Buy(){
		shopManager.Buy (itemClass);
	}

	public void Sell(){
		shopManager.Sell (itemClass);
	}


}
