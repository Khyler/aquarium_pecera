﻿using UnityEngine;
using System.Collections;

public class Tab : MonoBehaviour {

	public ITabable tabedPanel { get; set; }

	public int index{ get; set; }

	private UIButton button;

	public void SetEnabled(bool b){
		GetComponent<UIButton>().isEnabled = b;
	}


	public void SelectTab(){
		tabedPanel.ChangeTab (index);
	}
}
