﻿using UnityEngine;
using System.Collections;

public static class Constants {


	public static string[] itemSpriteNames= new string[8];
	public static int[][] itemSpriteSize= new int[8][];
	public static float[] itemPrice = new float[8];

	public static string[] fishSpriteNames= new string[7];
	public static float[] fishPrice = new float[7];


	static Constants(){
		itemSpriteNames [(int)ItemClass.TYPE.ancla] = "Item01";
		itemSpriteNames [(int)ItemClass.TYPE.barco] = "Item02";
		itemSpriteNames [(int)ItemClass.TYPE.vasijas] = "Item03";
		itemSpriteNames [(int)ItemClass.TYPE.faro] = "Item04";
		itemSpriteNames [(int)ItemClass.TYPE.piedra] = "Item05";
		itemSpriteNames [(int)ItemClass.TYPE.piedraPlantas] = "Item06";
		//itemSpriteNames [(int)ItemClass.TYPE.item7] = "Item01";
		//itemSpriteNames [(int)ItemClass.TYPE.item8] = "Item01";

		itemSpriteSize [(int)ItemClass.TYPE.ancla] = new int[]{162,155};
		itemSpriteSize [(int)ItemClass.TYPE.barco] = new int[]{338,380};
		itemSpriteSize [(int)ItemClass.TYPE.vasijas] = new int[]{160,118};
		itemSpriteSize [(int)ItemClass.TYPE.faro] = new int[]{90,360};
		itemSpriteSize [(int)ItemClass.TYPE.piedra] = new int[]{155,77};
		itemSpriteSize [(int)ItemClass.TYPE.piedraPlantas] = new int[]{245,170};
		//itemSpriteSize [(int)ItemClass.TYPE.item7] = new int[]{162,155};
		//itemSpriteSize [(int)ItemClass.TYPE.item8] = new int[]{162,155};

		itemPrice [(int)ItemClass.TYPE.ancla] = 500;
		itemPrice [(int)ItemClass.TYPE.barco] = 200;
		itemPrice [(int)ItemClass.TYPE.vasijas] = 300;
		itemPrice [(int)ItemClass.TYPE.faro] = 400;
		itemPrice [(int)ItemClass.TYPE.piedra] = 100;
		itemPrice [(int)ItemClass.TYPE.piedraPlantas] = 50;
		//itemPrice [(int)ItemClass.TYPE.item7] = 2000;
		//itemPrice [(int)ItemClass.TYPE.item8] = 10000;


		fishSpriteNames [(int)Fish.TYPE.Angel] = "Angel";
		fishSpriteNames [(int)Fish.TYPE.Cirujano] = "Cirujano";
		fishSpriteNames [(int)Fish.TYPE.Estandarte] = "Estandarte";
		fishSpriteNames [(int)Fish.TYPE.Verde] = "Verde";
		fishSpriteNames [(int)Fish.TYPE.Payaso] = "Payaso";
		fishSpriteNames [(int)Fish.TYPE.Cofre] = "Cofre";
		fishSpriteNames [(int)Fish.TYPE.Mandarin] = "Mandarin";


		fishPrice [(int)Fish.TYPE.Angel] = 100;
		fishPrice [(int)Fish.TYPE.Cirujano] = 50;
		fishPrice [(int)Fish.TYPE.Estandarte] = 100;
		fishPrice [(int)Fish.TYPE.Verde] = 25;
		fishPrice [(int)Fish.TYPE.Payaso] = 50;
		fishPrice [(int)Fish.TYPE.Cofre] = 200;
		fishPrice [(int)Fish.TYPE.Mandarin] = 200;
	}

}
