﻿using UnityEngine;
using System.Collections;

public class LanguageChangeButton : MonoBehaviour
{

	int languageIndex = 0;
	LanguageCode[] languages = {
		LanguageCode.EU,
		LanguageCode.ES,
		LanguageCode.EN,
		LanguageCode.FR
	};
	private UISprite sprite;
	private UIButton button;

	void Start ()
	{
		button = GetComponent<UIButton> ();
		sprite = GetComponent<UISprite> ();
		SetLanguageToCurrent ();
		UpdateLanguageIcon ();
	}

	public void NextLanguage ()
	{
		languageIndex = ((languageIndex + 1) % languages.Length);
		Language.SwitchLanguage (languages [languageIndex]);
		SetLanguageToCurrent ();
		UpdateLanguageIcon ();
	}

	private void SetLanguageToCurrent ()
	{
		foreach (GameObject obj in GameObject.FindGameObjectsWithTag("Localizable")) {
			obj.GetComponent<TextLocalization> ().UpdateText ();
		}
	}

	private void UpdateLanguageIcon ()
	{
		switch (Language.CurrentLanguage ()) {
		case LanguageCode.EU:
			{
				sprite.spriteName = "Euskera_Neutro";
				button.normalSprite = "Euskera_Neutro";
				button.hoverSprite = "Euskera_Select";
				languageIndex=0;
				break;
			}	
		case LanguageCode.ES:
			{
				sprite.spriteName = "Spanish_Neutro";
				button.normalSprite = "Spanish_Neutro";
			button.hoverSprite = "Spanish_Select";
			languageIndex=1;
				break;
			}
		case LanguageCode.EN:
			{
			sprite.spriteName = "Ingles_Neutro";			
			button.normalSprite = "Ingles_Neutro";
			button.hoverSprite = "Ingles_Select";
			languageIndex=2;
			break;
		}
		case LanguageCode.FR:
		{
			sprite.spriteName = "French_Neutro";
			button.normalSprite = "French_Neutro";
			button.hoverSprite = "French_Select";
			languageIndex=3;
				break;
		}
	}
	}
}
