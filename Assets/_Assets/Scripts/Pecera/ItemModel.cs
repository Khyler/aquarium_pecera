using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class ItemModel : MonoBehaviour{

	private const int MAX_X = 700;
	private const int MIN_X = -700;
	private const int MAX_Y = -95;
	private const int MIN_Y = -175;
	private const int MAX_Z = 500;
	private const int MIN_Z = -500;
	
	private const int X_DEPTH = 100;

	public InventoryManager inventoryManager{ get; set; }

	public Item itemData  { get; set; }
	public float alpha =1;

	private Ray ray;
	private RaycastHit hit;

	private ItemModel clone=null;
	private UISprite itemSprite;

	void Start() {

		itemSprite = GetComponent<UISprite> ();
		itemSprite.spriteName = Constants.itemSpriteNames [(int)itemData.type];
		itemSprite.width = Constants.itemSpriteSize [(int)itemData.type] [0];
		itemSprite.height = Constants.itemSpriteSize [(int)itemData.type] [1];


		UpdateColor();
		UpdateDepth();		
	}

	void OnDrag() {
		if (inventoryManager.isEnabled) {
			if (clone == null) {
				clone = inventoryManager.InstantiateGhostItemModel (itemData);
				/*clone.transform.parent=itemParent.transform;
			clone.transform.localPosition = transform.position;
			clone.transform.localEulerAngles = Vector3.zero;
			clone.transform.localScale = Vector3.one;
			Destroy (clone.GetComponent<ItemModel>());
			Destroy(clone.GetComponent<BoxCollider>());

			clone.GetComponent<UISprite>().alpha=0.5f;*/

			}

			//Debug.Log ("Draging");
			ray = Camera.main.ScreenPointToRay (Input.mousePosition);
		
			if (Physics.Raycast (ray, out hit) && hit.collider.CompareTag ("Shop")) {
				inventoryManager.MarkInventory ();
				clone.transform.position = hit.point;
			} else {
				inventoryManager.UnmarkInventory ();
				clone.CorrectPosition ();
			}
		}
	}


	 void OnDragEnd ()
	{
		if (inventoryManager.isEnabled) {
			ray = Camera.main.ScreenPointToRay (Input.mousePosition);

			if (Physics.Raycast (ray, out hit) && hit.collider.CompareTag ("Shop")) {
				inventoryManager.AddToInventory (this);
				GameObject.Destroy (gameObject);
			} else {
				inventoryManager.MoveInAquarium (this, clone.transform.localPosition);
				UpdateColor ();
				UpdateDepth ();
			}

			GameObject.Destroy (clone.gameObject);
			clone = null;

			inventoryManager.UnmarkInventory ();
		}
	}
	
	public void CorrectPosition(){
		Vector3 point = Camera.main.ScreenToWorldPoint(Input.mousePosition);
		
		transform.position = point;
		
		//Adjust localPosition;
		
		point = transform.localPosition;
		
		point.y = Mathf.Max(MIN_Y, point.y);
		point.y = Mathf.Min(MAX_Y, point.y);
		
		point.z = MIN_Z + ((point.y - MIN_Y) * (MAX_Z - MIN_Z) / (MAX_Y - MIN_Y));
		
		float zDepthPercentage = (MAX_Z - point.z) / (MAX_Z - MIN_Z);
		int width = GetComponent<UISprite>().width;
		
		point.x = Mathf.Max(MIN_X - (zDepthPercentage * X_DEPTH) + (width / 2), point.x);
		point.x = Mathf.Min(MAX_X + (zDepthPercentage * X_DEPTH) - (width / 2), point.x);
		
		transform.localPosition = point;
	}
	
	
	
	public void UpdateColor() {
		//float zDepthPercentage = (MAX_Z - obj.transform.localPosition.z) / (MAX_Z - MIN_Z);
		//obj.GetComponent<UISprite>().color = new Color(0.8f + (0.2f * zDepthPercentage), 0.9f + (0.1f * zDepthPercentage), 1,alpha); 
		GetComponent<UISprite> ().color = new Color (1, 1, 1, alpha);
	}
	
	
	
	public void UpdateDepth()
	{
		float zDepthPercentage = (MAX_Z - transform.localPosition.z) / (MAX_Z - MIN_Z);
		GetComponent<UISprite>().depth = (int)(zDepthPercentage * 100);
	}

}
