﻿using UnityEngine;
using System.Collections;

public interface ITabable  {

	void ChangeTab (int index);

}