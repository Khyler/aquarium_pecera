﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ItemClass  {

	public enum TYPE{
		barco,
		ancla,
		vasijas,	
		faro,
		piedra,
		piedraPlantas,
		//item7,
		//item8,
	}

	public TYPE type{ get; set; }

	public int onInventory { get; set; }

	public List<Item> items { get; set; }

	public ItemClass(){}

	public ItemClass(TYPE type){
		this.type= type;
		onInventory = 0;
		items = new List<Item> ();
	}
}
