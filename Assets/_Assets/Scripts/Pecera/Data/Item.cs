using UnityEngine;
using System.Collections;

public class Item {

	public ItemClass.TYPE type { get; set; }
	public bool onAquarium { get; set; }
	public float x {get; set;}
	public float y {get; set;}
	public float z {get; set;}

	public Item(){}

	public Item(ItemClass itemType){
		type = itemType.type;
		onAquarium = false;
		x = 0;
		y = 0;
		z = 0;
	}

}
