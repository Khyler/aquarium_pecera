using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class Inventory  {


	public List<ItemClass> itemClasses { get; set; }

	public Inventory (){
		itemClasses = new List<ItemClass> ();

		foreach (ItemClass.TYPE type in Enum.GetValues(typeof(ItemClass.TYPE))) {
			itemClasses.Add(new ItemClass(type));		
		}
		/*
		itemClasses = new List<ItemClass> ();

		ItemClass type;
		Item item;

		type = new ItemClass (ItemClass.TYPE.ancla, "ITEM_ANCLA", 1000);

		item = new Item (type);
		item.onAquarium = true;
		item.x = -22;
		item.y = -132;
		item.z = 0;
		type.items.Add (item);
		itemClasses.Add(type);


		type = new ItemClass (ItemClass.TYPE.barco, "ITEM_BARCO", 2500);
		item = new Item (type);
		item.onAquarium = true;
		item.x = 460;
		item.y = -132;
		item.z = 0;
		type.items.Add (item);
		itemClasses.Add(type);


		type = new ItemClass (ItemClass.TYPE.vasijas, "ITEM_VASIJAS", 500);
		item = new Item (type);
		item.onAquarium = true;
		item.x = -530;
		item.y = -132;
		item.z = 0;
		type.items.Add (item);
		itemClasses.Add(type);


		type = new ItemClass (ItemClass.TYPE.faro, "ITEM_FARO", 500);
		itemClasses.Add(type);

		type = new ItemClass (ItemClass.TYPE.item5, "ITEM_ANCLA", 123412);
		itemClasses.Add(type);

		type = new ItemClass (ItemClass.TYPE.item6, "ITEM_ANCLA", 213);
		itemClasses.Add(type);

		type = new ItemClass (ItemClass.TYPE.item7, "ITEM_ANCLA", 34);
		itemClasses.Add(type);

		type = new ItemClass (ItemClass.TYPE.item8, "ITEM_ANCLA", 432);
		itemClasses.Add(type);*/
	}
}
