﻿using UnityEngine;
using System.Collections;
using System;


public class Aquarium  {


	public Fishes fishes { get; set; }
	public Water water { get; set; }

	public Inventory inventory { get; set; }

	public float points { get; set; }


	public long lastSim { get; set; }


	public Aquarium(){
		fishes = new Fishes ();
		water = new Water ();
		inventory = new Inventory ();
		points = 5000;
		lastSim = DateTime.Now.Ticks/10000000;
	}

}
