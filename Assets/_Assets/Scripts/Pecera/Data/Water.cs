﻿using UnityEngine;
using System.Collections;

public class Water  {

	public const int FOODPILLS_INCREMENT = 5;
	public const int POOPPILLS_INCREMENT = 1;
	
	public const float FOODPILLS_COST=0.5f;
	public const float POOPPILLS_COST=20f;

	public float poopLevel{ get; set;}
	public float foodLevel{ get; set;}
	
	public float poopPills{ get; set;}
	public float foodPills{ get; set;}

	public Water(){
		poopLevel = 0;
		foodLevel = 0;

		poopPills = 0;
		foodPills = 0;
	}
}
