﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class Fishes {

	public List<Fish> adultFishes { get; set;}
	public List<Fish> childFishes { get; set;}

	public float[] bornFishes {get; set;}
	public float grownFishes {get; set;}
	public float deadFishes {get; set;}

	public Fishes(){
		adultFishes = new List<Fish> ();
		childFishes = new List<Fish> ();

		bornFishes = new float[Enum.GetValues(typeof(Fish.TYPE)).Length];
		grownFishes = 0;
		deadFishes = 0;
	}

}
