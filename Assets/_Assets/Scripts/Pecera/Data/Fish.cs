﻿using UnityEngine;
using System.Collections;

public class Fish
{
	
	public enum TYPE{
		Payaso,
		Cirujano,
		Estandarte,
		Cofre,
		Mandarin,
		Angel,
		Verde,
	}


	public TYPE type{ get; set; }
	public int typeVariant { get; set; }
	public bool adult { get; set; }
	public float sizeVariant { get; set; }

	public Fish (){}

	public Fish (TYPE type, int typeVariant){
		this.type= type;
		this.typeVariant = typeVariant;
		adult = false;
		sizeVariant= Random.Range (0.75f, 1.25f);
	}



}
