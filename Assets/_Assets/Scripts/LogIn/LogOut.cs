﻿using UnityEngine;
using System.Collections;
using Prime31;
#if UNITY_ANDROID
using FacebookAccess = Prime31.FacebookAndroid;
#elif UNITY_IPHONE
using FacebookAccess = Prime31.FacebookBinding;
#endif

public class LogOut : MonoBehaviour 
{
	public void OnClick()
	{
		Debug.Log("LOG OUT!");
		FacebookAccess.logout();
		Debug.Log("LOG OUT!");
	//	Application.LoadLevel ("LogIn");
	}
}
