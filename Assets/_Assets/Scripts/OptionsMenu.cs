﻿using UnityEngine;
using System.Collections;

public class OptionsMenu : MonoBehaviour {

	TweenPosition tw;
	UITexture texture;
	UILabel text;
	void Awake()
	{
		text = GetComponentInChildren<UILabel>();
		texture = GetComponentInChildren<UITexture>();
		//GetComponent<UIButton>().defaultColor = GetComponent<UITexture>().color;
		gameObject.SetActive(false);
		texture.alpha = 0.0f;
		tw = GetComponent<TweenPosition>();

		Vector3 tmpV =new Vector3(1f,0f,0f);

		tmpV.x = tmpV.x*GetComponent<UITexture>().width;
		tw.from = transform.localPosition - tmpV;
		tw.to = transform.localPosition;
		transform.localPosition = tw.from;
	}

	public void Show()
	{
		gameObject.SetActive(true);
		texture.alpha = 0.0f;
		texture.enabled = false;
		text.enabled = false;
		tw.PlayForward();
		StartCoroutine(DelayShow(tw.delay));
	}
	IEnumerator DelayShow(float t)
	{
		yield return new WaitForSeconds(t);
		texture.alpha = 1.0f;
		texture.enabled = true;
		text.enabled = true;

	}
	public void Hide()
	{
		tw.PlayReverse();
		StartCoroutine(DelayHide(tw.duration+tw.delay));
	}
	IEnumerator DelayHide(float t)
	{
		yield return new WaitForSeconds(t);
		texture.alpha = 0f;
		texture.enabled = false;
		text.enabled = false;
		//gameObject.SetActive(false);
	}


}
