using UnityEngine;
using System.Collections;

public class AntxoniAnim : MonoBehaviour {


	UISpriteAnimation anim;
	// Use this for initialization

	void Start () {
		anim = GetComponentInChildren<UISpriteAnimation> ();
		anim.framesPerSecond = 15;
		anim.ResetToBeginning ();
		anim.namePrefix="Entrada_";
		anim.Play();
		StartCoroutine ("CheckIdle");
	}
	

	private IEnumerator CheckIdle(){
		while (anim.isPlaying) {
			yield return new WaitForEndOfFrame();
		}
		anim.framesPerSecond=15;
		anim.namePrefix="Idle_";
		anim.loop=true;
		anim.Play();
	}
}
